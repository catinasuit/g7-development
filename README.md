## Synopsis

This is a set of code to show the beginnings of a REST API for managing Passports22

## Motivation

The project exists as a technical test written in a couple of hours, to see what can be done as a start.

## Installation

The project can be downloaded directly as a maven project.
Run as a maven build to produce a Snapshot of the Web Service
The Web Service can then be deployed to the Web Container of choice eg. Tomcat or Jetty
The Service can then be called on http://<container address>/PapersPlease/Passport/service to determine if the service is available

## API Reference

The API contains 5 separate calls. The API accepts json/application objects and will return a json/application response.
service()
addCustomer(Customer)
addPassport(Passport)
deletePassport(Passport)
getPassports(customerId)

## Tests

The project comes with junit tests.
If the project is deployed in a web container then it can be tested by sending trying the following URL
http://<container address>/PapersPlease/Passport/GetPassports/MarkMatthewLondon which will return a JSON array of data.

## Contributors

Just the one.

## License

There is no licence associated with this code and you are free to amend and use as you see fit.
