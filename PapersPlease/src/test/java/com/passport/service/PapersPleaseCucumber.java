package com.passport.service;



import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(format = { "html:target/cucumber-html-report","json:cucumber-json2report/data/cucumber.json" },
		features={"src/test/resources/PapersPlease.feature"},
		tags = { "@All"})
public class PapersPleaseCucumber {
	
}

