package com.passport.service;


import static org.junit.Assert.assertEquals;

import java.util.Date;

import javax.ws.rs.core.Response;

import org.junit.Test;

import com.passport.model.Customer;
import com.passport.model.Passport;

public class PapersPleaseTest {



	PassportService service = new PassportService();

	@Test
	public void serviceTest(){

		int replyStatus = service.getService().getStatus();
		assertEquals("Service Check", Response.Status.OK.getStatusCode(), replyStatus);
	}

	
	@Test
	public void addCustomerTest(){
		
		Customer c = new Customer();
		c.setFirstName("Mark");
		c.setLastName("Matthew");
		c.setDateOfBirth(new Date());
		c.setLocationOfBirth("London");
		
		String cID = new StringBuffer().append(c.getFirstName())
				.append(c.getLastName())
				.append(c.getLocationOfBirth()).toString();
		
		Response reply = service.addCustomer(c);

		assertEquals("Service Check", Response.Status.OK.getStatusCode(), reply.getStatus());
		assertEquals("Customer ID Check", cID, reply.getEntity());
		
	}


	@Test
	public void addPassportTest(){
		
		Passport p = new Passport();
		p.setIssueDate(new Date());
		p.setPassportNumber("ABC123");
		Response reply = service.addPassport(p);
		
		assertEquals("Service Check", Response.Status.NO_CONTENT.getStatusCode(), reply.getStatus());
		
	}


	@Test
	public void deleteExistingPassportTest(){
		
		Passport p = new Passport();
		p.setIssueDate(new Date());
		p.setPassportNumber("ABC123");
		Response reply = service.deletePassport(p);
		assertEquals("Delete Known Passport Status", Response.Status.OK.getStatusCode(), reply.getStatus());
	}

	@Test
	public void deleteUnknownPassportTest(){
		
		Passport p = new Passport();
		p.setIssueDate(new Date());
		p.setPassportNumber("DEF456");
		Response reply = service.deletePassport(p);
		assertEquals("Delete Unknown Passport Status", Response.Status.NOT_FOUND.getStatusCode(), reply.getStatus());
	}

	@Test
	public void getKnownPassportsTest(){

		String customerID = "MarkMatthewLondon";

		Response reply = service.getPassports(customerID);
		assertEquals("Known Passports Status", Response.Status.OK.getStatusCode(), reply.getStatus());

	}

	@Test
	public void getNoPassportsTest(){

		String customerID = "LukeJohnCanada";

		Response reply = service.getPassports(customerID);

		assertEquals("No Passports Status", Response.Status.NOT_FOUND.getStatusCode(), reply.getStatus());
	}

}
