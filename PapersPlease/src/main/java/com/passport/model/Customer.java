package com.passport.model;

import java.util.Date;
import java.util.List;

public class Customer {

	private String customerID;
	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private String locationOfBirth;
	private String gender;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getLocationOfBirth() {
		return locationOfBirth;
	}
	
	public void setLocationOfBirth(String locationOfBirth) {
		this.locationOfBirth = locationOfBirth;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
		
}
