package com.passport.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import com.passport.controller.PassportController;
import com.passport.controller.PassportControllerImpl;
import com.passport.model.Customer;
import com.passport.model.Passport;

@Path("/Passport")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PassportService {

		PassportController controller = new PassportControllerImpl();
	
		@GET
		@Path("/service")
		public Response getService(){
			
			return Response.status(200).entity("Service Available").build();
			
		}
	
		@POST
		@Path("/AddCustomer")
		public Response addCustomer(Customer c){
			
			Status status = Response.Status.OK;
			String cId = controller.addCustomer(c);
			
			if (cId == null){
				status = Response.Status.BAD_REQUEST;
			}
			
			return Response.status(status).entity(cId).build();
			
			
		}

		@PUT
		@Path("/AddPassport")
		public Response addPassport(Passport p){
			
			Status status = Response.Status.NO_CONTENT;
			
			boolean success  = controller.addPassport(p);
			
			if (!success){
				status = Response.Status.BAD_REQUEST;
			}
			
			return Response.status(status).build();
			
		}

		@DELETE
		@Path("/DeletePassport")
		public Response deletePassport(Passport p){
			
			Status status = Response.Status.OK;
			boolean success  = controller.deletePassport(p);
			
			if (!success){
				status = Response.Status.NOT_FOUND;
			}
			
			return Response.status(status).build();
			
			
		}

		@GET
		@Path("/GetPassports/{cID}")
		public Response getPassports(@PathParam("cID") String customerId){
			
			Status status = Response.Status.OK;

			JSONArray jsonArray = new JSONArray();
			List<Passport> passports = controller.getPassports(customerId);
			
			if (passports == null) {
				status = Response.Status.BAD_REQUEST;
			} else if (passports.size() == 0) {
				status = Response.Status.NOT_FOUND;
			} else {
				for (Passport p : passports){
					JSONObject obj = new JSONObject();
					obj.put("number", p.getPassportNumber());
					obj.put("issueDate", p.getIssueDate());
					jsonArray.put(obj);
				}
				
				
			}
			
			return Response.status(status).entity(jsonArray.toString()).build();

		}


}
