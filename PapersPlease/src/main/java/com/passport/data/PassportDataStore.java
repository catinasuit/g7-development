package com.passport.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.passport.model.Customer;
import com.passport.model.Passport;

public class PassportDataStore {

	private static final PassportDataStore store = new PassportDataStore();
	private HashMap<String,Customer> customers = new HashMap<String,Customer>();
	private HashMap<String,Passport> passports = new HashMap<String,Passport>();	
	private HashMap<String, List<Passport>> passportList = new HashMap<String, List<Passport>>();
	
	
	
	private PassportDataStore(){
		
		createSetupData();		
	}
	
	public static PassportDataStore getInstance(){
				
		return store;
	}
	
	
	public String insertCustomer(Customer c){
		
		String cHash = getCustomerKey(c);
		
		if (!customers.containsKey(cHash)){
			customers.put(cHash, c);
		} 
		
		return cHash;
	}
	
	public boolean insertPassport(Passport p){
		
		
		if (!passports.containsKey(p.getPassportNumber())){
			passports.put(p.getPassportNumber(),p);
		} 
		
		return true;
	}
	
	public boolean deletePassport( Passport p){
		
		boolean success = false;
		
		if (passports.containsKey(p.getPassportNumber())){
			passports.remove(p.getPassportNumber(),p);
			success = true;
		} 
		
		return success;
	}
	
	public List<Passport> selectPassports(String customerID){
		
		List<Passport> passports = null;
		
		if (passportList.containsKey(customerID)){
			passports = passportList.get(customerID);
		}
		return passports;
	}
	
	private void createSetupData(){
		
		Customer custOne = new Customer();
		custOne.setFirstName("Mark");
		custOne.setLastName("Matthew");
		custOne.setDateOfBirth(new Date());
		custOne.setLocationOfBirth("London");
		custOne.setCustomerID(getCustomerKey(custOne));
		insertCustomer(custOne);

		Customer custTwo = new Customer();
		custTwo.setFirstName("Luke");
		custTwo.setLastName("John");
		custTwo.setDateOfBirth(new Date());
		custTwo.setLocationOfBirth("Canada");
		custTwo.setCustomerID(getCustomerKey(custTwo));
		insertCustomer(custTwo);
		
		Passport passOne = new Passport();
		passOne.setPassportNumber("ABC123");
		passOne.setIssueDate(new Date());
		
		Passport passTwo = new Passport();
		passTwo.setPassportNumber("456RST");
		passTwo.setIssueDate(new Date());
		
		passports.put(passOne.getPassportNumber(), passOne);
		passports.put(passTwo.getPassportNumber(), passTwo);
		
		List<Passport> custOneList = new ArrayList<Passport>();
		custOneList.add(passOne);
		custOneList.add(passTwo);
		passportList.put(custOne.getCustomerID(), custOneList);

		List<Passport> custTwoList = new ArrayList<Passport>();
		passportList.put(custTwo.getCustomerID(), custTwoList);
		
		
	}
	
	private String getCustomerKey(Customer c) {
		return new StringBuffer().append(c.getFirstName())
						.append(c.getLastName())
						.append(c.getLocationOfBirth()).toString();

	}
	
}
