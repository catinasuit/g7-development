package com.passport.controller;

import java.util.List;

import com.passport.data.PassportDataStore;
import com.passport.exception.DataException;
import com.passport.model.Customer;
import com.passport.model.Passport;

public class DataControllerImpl implements DataController {

	
	
	@Override
	public String insertCustomer(Customer c) throws DataException {

		return PassportDataStore.getInstance().insertCustomer(c);
	

	}

	@Override
	public boolean insertPassport(Passport p) throws DataException {
		
		return PassportDataStore.getInstance().insertPassport(p);
		// TODO Auto-generated method stub

	}

	@Override
	public boolean deletePassport(Passport p) throws DataException {
		
		return PassportDataStore.getInstance().deletePassport(p);
		

	}

	@Override
	public List<Passport> selectPassports(String customerId) throws DataException {
		return PassportDataStore.getInstance().selectPassports(customerId);
		// TODO Auto-generated method stub

	}

}
