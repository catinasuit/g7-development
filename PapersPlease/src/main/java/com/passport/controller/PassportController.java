package com.passport.controller;

import java.util.List;

import com.passport.model.Customer;
import com.passport.model.Passport;

public interface PassportController {

	String addCustomer(Customer c);
	
	boolean addPassport(Passport p);
	
	boolean deletePassport(Passport p);
	
	List<Passport> getPassports(String customerID);
	
}
