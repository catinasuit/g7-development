package com.passport.controller;

import java.util.List;

import com.passport.exception.DataException;
import com.passport.model.Customer;
import com.passport.model.Passport;

public class PassportControllerImpl implements PassportController {

	DataController dataSource = new DataControllerImpl();
	
	@Override
	public String addCustomer(Customer c) {
		
		String cId = null;
			
		try{
			cId = dataSource.insertCustomer(c);
		} catch (DataException dE) {
			return null;
		}
		// TODO Auto-generated method stub
		return cId;
	}

	@Override
	public boolean addPassport(Passport p) {

		boolean success = false;
		
		try{
			success = dataSource.insertPassport(p);;
		} catch (DataException dE) {
			success = false;

		}
		// TODO Auto-generated method stub
		return success;
	}

	@Override
	public boolean deletePassport(Passport p) {


		boolean success = false;
		
		try{
			success = dataSource.deletePassport(p);
		} catch (DataException dE) {
			success = false;

		}
		
		return success;
	}

	@Override
	public List<Passport> getPassports(String customerID) {


		List<Passport> passports = null;
		
		try{
			passports = dataSource.selectPassports(customerID);
		} catch (DataException dE) {
			passports = null;

		}
		
		return passports;
	}

}
