package com.passport.controller;

import java.util.List;

import com.passport.exception.DataException;
import com.passport.model.Customer;
import com.passport.model.Passport;

public interface DataController {

	String insertCustomer(Customer c) throws DataException;
	
	boolean insertPassport(Passport p) throws DataException;
	
	boolean deletePassport(Passport p) throws DataException;
	
	List<Passport> selectPassports(String customerID) throws DataException;
	
	
}
