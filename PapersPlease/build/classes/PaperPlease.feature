Feature: Paper Please Testing

    @Service  @All
    Scenario: Test service is available
    Given I have no text to submit
    And the path is "service"
    When I get the request
    Then the status should be 200

    @Add @All  
    Scenario: Add a Customer
    Given a valid customer is provided
    And the path is "AddCustomer"
    When I post the request
    Then the status should be 200
    
    @Add @All
    Scenario: Add a Passport
    Given a valid passport is provided
    And the path is "AddPassport"
    When I post the request
    Then the status should be 200

    @Delete @All
    Scenario: Delete a Passport
    Given an existing passport is provided
    And the path is "DeletePassport"
    When I post the request
    Then the status should be 200
	
    @Delete @All
    Scenario: Delete a Passport
    Given an non-existing passport is provided
    And the path is "DeletePassport"
    When I post the request
    Then the status should be 500
    
    @Get @All 
    Scenario: Retrieve passports for a user
    Given a valid customer is provided
    And the path is "GetPassports"
    When I get the request
    Then the status should be 200
    And the response should match "ValidPassport.json" 

